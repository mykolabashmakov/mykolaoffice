﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MykolaEdit
{
    class TabManager
    {
        private TabControl tabControl = null;
        private List<FileManager> managed_files = new List<FileManager>();
        public TabManager(TabControl tc)
        {
            tabControl = tc;
        }

        public void AddNewTab(string filepath = null)
        {
            var tmpTabPage = new TabPage();
            var tmpRTB = new RichTextBox();
            tmpTabPage.Controls.Add(tmpRTB);
            tmpTabPage.Font = new System.Drawing.Font("Monospac821 BT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tmpTabPage.Location = new System.Drawing.Point(4, 22);
            tmpTabPage.Padding = new System.Windows.Forms.Padding(3);
            tmpTabPage.Size = new System.Drawing.Size(768, 385);
            tmpTabPage.UseVisualStyleBackColor = true;
            
            tmpRTB.Location = new System.Drawing.Point(6, 3);
            tmpRTB.Size = new System.Drawing.Size(756, 376);

            managed_files.Add(new FileManager(tmpTabPage, filepath));
            tabControl.TabPages.Add(tmpTabPage);
        }

        public void SaveActiveTab()
        {
            var fm = GetActiveFileManager();
            if (fm != null)
            {
                if (fm.IsPathNull())
                {
                    SaveActiveTabWithPrompt();
                    return;
                }
                fm.SaveFile();
                System.Windows.Forms.MessageBox.Show("Saved!");
            }
        }

        public void SaveActiveTabWithPrompt()
        {
            var fm = GetActiveFileManager();
            if (fm != null)
            {
                string path = GetSavePath();
                if (path != null)
                {
                    fm.SaveFileAs(path);
                }
            }
            System.Windows.Forms.MessageBox.Show("Saved!");
        }


        string GetSavePath()
        {
            FileStream myStream;

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = (FileStream)saveFileDialog1.OpenFile()) != null)
                {
                    myStream.Close();
                    return myStream.Name;
                }
            }
            return null;
        }


        private FileManager GetActiveFileManager()
        {
            foreach(var fm in managed_files)
            {
                if (tabControl.SelectedTab == fm.tabPage)
                {
                    return fm;
                }
            }
            return null;
        }

        public static IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        public void AlertUnsaved(FormClosingEventArgs e)
        {
            foreach (var fm in managed_files)
            {
                if (!fm.saved)
                {
                    DialogResult dialogResult = MessageBox.Show($"{fm.tabPage.Text} file isn`t saved. All changes since last save will be lost!\nAre you sure?", "You Have Unsaved Files!", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        // Ok just skip 
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }
        public void HighlightWordsFromFirstNSentences()
        {
            var fm = GetActiveFileManager();
            fm.HighlightWordsFromFirstNSentences();
        }

        public void ClearHighlightOnActiveWindow()
        {
            var fm = GetActiveFileManager();
            fm.ClearHighlight();
        }

        public void SearchSelectedWordFromActiveTab()
        {
            var fm = GetActiveFileManager();
            var word = fm.GetSelectedText().Trim().Split().First().ToLower();
            if (word.Length < 2)
            {
                MessageBox.Show("Selected word`s length must be greater or equal 2");
                return;
            }

            var words = GetSearchableWords();
            List<string> filtered = words;
            int cnt = 1;
            while(filtered.Count != 0 && cnt <= word.Length)
            {
                var subword = word.Substring(0, cnt);
                words = filtered;
                filtered = words.FindAll(w => w.StartsWith(subword));
                cnt++;
            }
            var text = String.Join("\n", words.Take(10));
            MessageBox.Show(text, $"Search Results: {word}");
        }

        private List<string> GetSearchableWords()
        {
            
            return File.ReadAllLines("./test_lst.txt").ToList().ConvertAll(s => s.ToLower());
        }
    }
}
