﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MykolaEdit
{
    public partial class Form1 : Form
    {
        TabManager tm;
        public Form1()
        {
            InitializeComponent();
            tm = new TabManager(tabControl1);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();
            foreach (var arg in args.Skip(1))
            {
                tm.AddNewTab(arg);
            }
        }
        private TabPage CreateNewTabPage()
        {
            return null;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tm.SaveActiveTab();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tm.SaveActiveTabWithPrompt();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            tm.AlertUnsaved(e);
        }

        private void highlightWordsFromFirst3SentencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tm.HighlightWordsFromFirstNSentences();
        }

        private void clearHighlightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tm.ClearHighlightOnActiveWindow();
        }

        private void searchSelectedWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tm.SearchSelectedWordFromActiveTab();
        }
    }
}
