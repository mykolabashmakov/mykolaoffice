﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MykolaEdit
{
    class FileManager
    {
        private string filepath = null;
        public TabPage tabPage = null;
        private RichTextBox textBox = null;
        public bool saved = false;

        public object CreateText { get; private set; }

        public FileManager(TabPage tabPage_, string filepath_ = null)
        {
            tabPage = tabPage_;
            textBox = (RichTextBox)TabManager.GetAll(tabPage, typeof(RichTextBox)).First();
            filepath = filepath_;
            textBox.TextChanged += new System.EventHandler(
                (object sender, System.EventArgs e) => {
                    this.saved = false;
                });
            if (filepath != null)
            {
                textBox.Text = GetFileTextOrCreate();
                tabPage.Text = GetFileName();
                saved = true;
            }
        }

        public void SaveFile()
        {
            try
            {
                File.WriteAllText(filepath, textBox.Text);
                saved = true;
            } catch (Exception ex)
            {

            } 
        }

        public void SaveFileAs(string filepath_ = null )
        {
            try
            {
                File.WriteAllText(filepath_, textBox.Text);
                saved = true;
                if (filepath == null)
                {
                    filepath = filepath_;
                    tabPage.Text = GetFileName();
                }
            } catch (Exception ex)
            {

            }
        }

        public string GetFileTextOrCreate()
        {
            if (File.Exists(filepath))
            {
                return File.ReadAllText(filepath);
            }
            var file = File.Create(filepath);
            file.Close();
            return null;
        }

        public string GetFileName()
        {
            if (filepath == null)
            {
                return "<Unnamed File>";
            }
            using (var file = File.OpenRead(filepath))
            {
                return Path.GetFileName(file.Name);
            }
        }

        public bool IsPathNull()
        {
            return filepath == null;
        }


        public void HighlightWordsFromFirstNSentences(int n = 3)
        {
            string[] sentences = Regex.Split(textBox.Text, @"(?<=[\.!\?])\s+", RegexOptions.Multiline);
            HashSet<string> words = new HashSet<string>();
            foreach (var s in sentences.Take(n))
            {
                var punctuation = s.Where(Char.IsPunctuation).Distinct().ToArray();
                var words_ = s.Split().Select(x => x.Trim(punctuation));
                words.UnionWith(words_);
            }
            foreach (var word in words)
            {
                HighlightText(word);
            }
        }


        public void ClearHighlight()
        {
            textBox.Select(0, textBox.TextLength);
            textBox.SelectionBackColor = Color.Transparent;
        }


        public void HighlightText(string word)
        {
            Color color = Color.Yellow;

            if (word == string.Empty)
                return;

            int s_start = textBox.SelectionStart, startIndex = 0, index;

            while ((index = textBox.Text.IndexOf(word, startIndex)) != -1)
            {
                textBox.Select(index, word.Length);
                textBox.SelectionBackColor = color;

                startIndex = index + word.Length;
            }

            textBox.SelectionStart = s_start;
            textBox.SelectionLength = 0;
            textBox.SelectionBackColor = Color.Transparent;
        }
        
        public string GetSelectedText()
        {
            return textBox.SelectedText;   
        }
    }
}
