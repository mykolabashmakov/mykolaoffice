﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml.Linq;

namespace XMLtoScreen
{
    class LINQSearch : ISearchStrategy
    {
        private XDocument xdoc = new XDocument();
        private List<Paper> result = null;
        public List<Paper> Search(Paper filter, string path = null)
        {
            xdoc = XDocument.Load(@path);
            result = new List<Paper>();
            List<XElement> matches = (
                from val in xdoc.Descendants("paper")
                where (
                        (filter.author      == null || filter.author     == val.Attribute("author").Value)      &&
                        (filter.faculty     == null || filter.faculty    == val.Attribute("faculty").Value)     &&
                        (filter.department  == null || filter.department == val.Attribute("department").Value)  &&
                        (filter.laboratory  == null || filter.laboratory == val.Attribute("laboratory").Value)  &&
                        (filter.office      == null || filter.office     == val.Attribute("office").Value)      &&
                        (filter.customer    == null || filter.customer   == val.Attribute("customer").Value)    &&
                        (filter.area        == null || filter.area       == val.Attribute("area").Value) 
                    ) select val
                ).ToList();
            foreach(XElement elem in matches)
            {
                List<string> eData = new List<string>(12);
                eData.Add(elem.Attribute("id").Value);
                eData.Add(elem.Attribute("author").Value);
                eData.Add(elem.Attribute("name").Value);
                eData.Add(elem.Attribute("faculty").Value);
                eData.Add(elem.Attribute("department").Value);
                eData.Add(elem.Attribute("laboratory").Value);
                eData.Add(elem.Attribute("office").Value);
                eData.Add(elem.Attribute("officeStart").Value);
                eData.Add(elem.Attribute("officeEnd").Value);
                eData.Add(elem.Attribute("customer").Value);
                eData.Add(elem.Attribute("subordination").Value);
                eData.Add(elem.Attribute("area").Value);
                result.Add(new Paper(eData));
            }                                   
            return result;
            //throw new NotImplementedException();
        }
    }
}
