﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Xsl;

namespace XMLtoScreen
{
    public partial class XMLtoScreen : Form
    {
        Context context = new Context(new LINQSearch());
        public XMLtoScreen()
        {
            InitializeComponent();
            List<Paper> papers = context.Search(new Paper());
            Dictionary<string, HashSet<string>> uniqueAttributes = Paper.GetUniqueAttributes(papers);
            comboBoxAuthor.Items.AddRange(uniqueAttributes["author"].ToArray());
            comboBoxFaculty.Items.AddRange(uniqueAttributes["faculty"].ToArray());
            comboBoxDepartment.Items.AddRange(uniqueAttributes["department"].ToArray());
            comboBoxLaboratory.Items.AddRange(uniqueAttributes["laboratory"].ToArray());
            comboBoxOffice.Items.AddRange(uniqueAttributes["office"].ToArray());
            comboBoxCustomer.Items.AddRange(uniqueAttributes["customer"].ToArray());
            comboBoxArea.Items.AddRange(uniqueAttributes["area"].ToArray());
        }

        public Paper GetPaperFilter()
        {
            var fData = new List<string>(12);
            for (int i = 0; i < 12; ++i)
                fData.Add(null);
            //fData[0] = null;
            if (checkBoxAuthor.Checked)
                fData[1] = comboBoxAuthor.Text;
            //fData[2] = null;
            if (checkBoxFaculty.Checked)
                fData[3] = comboBoxFaculty.Text;
            if (checkBoxDepartment.Checked)
                fData[4] = comboBoxDepartment.Text;
            if (checkBoxLaboratory.Checked)
                fData[5] = comboBoxLaboratory.Text;
            if (checkBoxOffice.Checked)
                fData[6] = comboBoxOffice.Text;
            //fData[7] = null;
            //fData[8] = null;
            if (checkBoxCustomer.Checked)
                fData[9] = comboBoxCustomer.Text;
            if (checkBoxArea.Checked)
                fData[11] = comboBoxArea.Text;
            return new Paper(fData);
        }

        private void DisplayOnScreen(List<Paper> papers)
        {
            richTextBoxDisplay.Clear();
            foreach (Paper p in papers)
            {
                richTextBoxDisplay.AppendText("<Paper\n");
                richTextBoxDisplay.AppendText("\tid = " + p.id + "\n");
                richTextBoxDisplay.AppendText("\tauthor = " + p.author + "\n");
                richTextBoxDisplay.AppendText("\tname = " + p.name + "\n");
                richTextBoxDisplay.AppendText("\tfaculty = " + p.faculty + "\n");
                richTextBoxDisplay.AppendText("\tdepartment = " + p.department + "\n");
                richTextBoxDisplay.AppendText("\tlaboratory = " + p.laboratory + "\n");
                richTextBoxDisplay.AppendText("\toffice = " + p.office + "\n");
                richTextBoxDisplay.AppendText("\tofficeStart = " + p.officeStart + "\n");
                richTextBoxDisplay.AppendText("\tofficeEnd = " + p.officeEnd + "\n");
                richTextBoxDisplay.AppendText("\tcustomer = " + p.customer + "\n");
                richTextBoxDisplay.AppendText("\tsubordination = " + p.subordination + "\n");
                richTextBoxDisplay.AppendText("\tarea = " + p.area + " \\>,\n");
            }
        }

        public void Search()
        {
            if (radioButtonLINQ.Checked)
            {
                context.SetStrategy(new LINQSearch());
            }
            else if (radioButtonDOM.Checked)
            {
                context.SetStrategy(new DOMSearch());
            } else if (radioButtonSAX.Checked)
            {
                context.SetStrategy(new SAXSearch());
            }
            Paper filter = GetPaperFilter();
            List<Paper> result = context.Search(filter);
            DisplayOnScreen(result);
        }

        public void Help()
        {
            MessageBox.Show("Firstly, select search criteria\n" +
                "Press APPLY\n" +
                "Ctrl+E to export to HTML\n" +
                "Ctrl+O to open exported HTML\n" +
                "Ctrl+H to open this window");
        }

        private void toHTMLToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            XslCompiledTransform xslt = new XslCompiledTransform();
            xslt.Load(Constants.xsltPath);
            string result = @"out.html";
            xslt.Transform(Constants.FullFilePath, result);
        }

        private void openHTMLToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            _ = System.Diagnostics.Process.Start("out.html");
        }

        private void buttonApply_Click(object sender, System.EventArgs e)
        {
            Search();
        }

        private void checkBoxAuthor_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxAuthor.Checked)
            {
                comboBoxAuthor.Enabled = true;
            } else
            {
                comboBoxAuthor.Enabled = false;
            }
        }

        private void checkBoxFaculty_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxFaculty.Checked)
            {
                comboBoxFaculty.Enabled = true;
            }
            else
            {
                comboBoxFaculty.Enabled = false;
            }
        }

        private void checkBoxDepartment_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxDepartment.Checked)
            {
                comboBoxDepartment.Enabled = true;
            }
            else
            {
                comboBoxDepartment.Enabled = false;
            }
        }

        private void checkBoxLaboratory_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxLaboratory.Checked)
            {
                comboBoxLaboratory.Enabled = true;
            }
            else
            {
                comboBoxLaboratory.Enabled = false;
            }
        }

        private void checkBoxOffice_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxOffice.Checked)
            {
                comboBoxOffice.Enabled = true;
            }
            else
            {
                comboBoxOffice.Enabled = false;
            }
        }

        private void checkBoxCustomer_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxCustomer.Checked)
            {
                comboBoxCustomer.Enabled = true;
            }
            else
            {
                comboBoxCustomer.Enabled = false;
            }
        }

        private void checkBoxArea_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxArea.Checked)
            {
                comboBoxArea.Enabled = true;
            }
            else
            {
                comboBoxArea.Enabled = false;
            }
        }

        private void helpToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Help();
        }

        private void richTextBoxDisplay_TextChanged(object sender, System.EventArgs e)
        {

        }
    }
}
