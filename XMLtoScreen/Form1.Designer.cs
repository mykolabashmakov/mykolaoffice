﻿namespace XMLtoScreen
{
    partial class XMLtoScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonSAX = new System.Windows.Forms.RadioButton();
            this.radioButtonDOM = new System.Windows.Forms.RadioButton();
            this.radioButtonLINQ = new System.Windows.Forms.RadioButton();
            this.boxMethod = new System.Windows.Forms.GroupBox();
            this.comboBoxAuthor = new System.Windows.Forms.ComboBox();
            this.boxFilters = new System.Windows.Forms.GroupBox();
            this.checkBoxArea = new System.Windows.Forms.CheckBox();
            this.comboBoxArea = new System.Windows.Forms.ComboBox();
            this.checkBoxCustomer = new System.Windows.Forms.CheckBox();
            this.comboBoxCustomer = new System.Windows.Forms.ComboBox();
            this.checkBoxOffice = new System.Windows.Forms.CheckBox();
            this.comboBoxOffice = new System.Windows.Forms.ComboBox();
            this.checkBoxLaboratory = new System.Windows.Forms.CheckBox();
            this.comboBoxLaboratory = new System.Windows.Forms.ComboBox();
            this.checkBoxDepartment = new System.Windows.Forms.CheckBox();
            this.comboBoxDepartment = new System.Windows.Forms.ComboBox();
            this.checkBoxFaculty = new System.Windows.Forms.CheckBox();
            this.comboBoxFaculty = new System.Windows.Forms.ComboBox();
            this.checkBoxAuthor = new System.Windows.Forms.CheckBox();
            this.buttonApply = new System.Windows.Forms.Button();
            this.boxMenu = new System.Windows.Forms.GroupBox();
            this.richTextBoxDisplay = new System.Windows.Forms.RichTextBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toHTMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openHTMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boxMethod.SuspendLayout();
            this.boxFilters.SuspendLayout();
            this.boxMenu.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButtonSAX
            // 
            this.radioButtonSAX.AutoSize = true;
            this.radioButtonSAX.Location = new System.Drawing.Point(238, 19);
            this.radioButtonSAX.Name = "radioButtonSAX";
            this.radioButtonSAX.Size = new System.Drawing.Size(46, 17);
            this.radioButtonSAX.TabIndex = 0;
            this.radioButtonSAX.Text = "SAX";
            this.radioButtonSAX.UseVisualStyleBackColor = true;
            // 
            // radioButtonDOM
            // 
            this.radioButtonDOM.AutoSize = true;
            this.radioButtonDOM.Location = new System.Drawing.Point(122, 19);
            this.radioButtonDOM.Name = "radioButtonDOM";
            this.radioButtonDOM.Size = new System.Drawing.Size(50, 17);
            this.radioButtonDOM.TabIndex = 1;
            this.radioButtonDOM.Text = "DOM\r\n";
            this.radioButtonDOM.UseVisualStyleBackColor = true;
            // 
            // radioButtonLINQ
            // 
            this.radioButtonLINQ.AutoSize = true;
            this.radioButtonLINQ.Checked = true;
            this.radioButtonLINQ.Location = new System.Drawing.Point(6, 19);
            this.radioButtonLINQ.Name = "radioButtonLINQ";
            this.radioButtonLINQ.Size = new System.Drawing.Size(50, 17);
            this.radioButtonLINQ.TabIndex = 2;
            this.radioButtonLINQ.TabStop = true;
            this.radioButtonLINQ.Text = "LINQ";
            this.radioButtonLINQ.UseVisualStyleBackColor = true;
            // 
            // boxMethod
            // 
            this.boxMethod.Controls.Add(this.radioButtonSAX);
            this.boxMethod.Controls.Add(this.radioButtonLINQ);
            this.boxMethod.Controls.Add(this.radioButtonDOM);
            this.boxMethod.Location = new System.Drawing.Point(6, 236);
            this.boxMethod.Name = "boxMethod";
            this.boxMethod.Size = new System.Drawing.Size(290, 50);
            this.boxMethod.TabIndex = 3;
            this.boxMethod.TabStop = false;
            this.boxMethod.Text = "Method";
            // 
            // comboBoxAuthor
            // 
            this.comboBoxAuthor.Enabled = false;
            this.comboBoxAuthor.FormattingEnabled = true;
            this.comboBoxAuthor.Location = new System.Drawing.Point(109, 17);
            this.comboBoxAuthor.Name = "comboBoxAuthor";
            this.comboBoxAuthor.Size = new System.Drawing.Size(175, 21);
            this.comboBoxAuthor.TabIndex = 4;
            // 
            // boxFilters
            // 
            this.boxFilters.Controls.Add(this.checkBoxArea);
            this.boxFilters.Controls.Add(this.comboBoxArea);
            this.boxFilters.Controls.Add(this.checkBoxCustomer);
            this.boxFilters.Controls.Add(this.comboBoxCustomer);
            this.boxFilters.Controls.Add(this.checkBoxOffice);
            this.boxFilters.Controls.Add(this.comboBoxOffice);
            this.boxFilters.Controls.Add(this.checkBoxLaboratory);
            this.boxFilters.Controls.Add(this.comboBoxLaboratory);
            this.boxFilters.Controls.Add(this.checkBoxDepartment);
            this.boxFilters.Controls.Add(this.comboBoxDepartment);
            this.boxFilters.Controls.Add(this.checkBoxFaculty);
            this.boxFilters.Controls.Add(this.comboBoxFaculty);
            this.boxFilters.Controls.Add(this.checkBoxAuthor);
            this.boxFilters.Controls.Add(this.comboBoxAuthor);
            this.boxFilters.Location = new System.Drawing.Point(6, 19);
            this.boxFilters.Name = "boxFilters";
            this.boxFilters.Size = new System.Drawing.Size(290, 211);
            this.boxFilters.TabIndex = 5;
            this.boxFilters.TabStop = false;
            this.boxFilters.Text = "Filters";
            // 
            // checkBoxArea
            // 
            this.checkBoxArea.AutoSize = true;
            this.checkBoxArea.Location = new System.Drawing.Point(6, 182);
            this.checkBoxArea.Name = "checkBoxArea";
            this.checkBoxArea.Size = new System.Drawing.Size(48, 17);
            this.checkBoxArea.TabIndex = 18;
            this.checkBoxArea.Text = "Area";
            this.checkBoxArea.UseVisualStyleBackColor = true;
            this.checkBoxArea.CheckedChanged += new System.EventHandler(this.checkBoxArea_CheckedChanged);
            // 
            // comboBoxArea
            // 
            this.comboBoxArea.Enabled = false;
            this.comboBoxArea.FormattingEnabled = true;
            this.comboBoxArea.Location = new System.Drawing.Point(109, 180);
            this.comboBoxArea.Name = "comboBoxArea";
            this.comboBoxArea.Size = new System.Drawing.Size(175, 21);
            this.comboBoxArea.TabIndex = 17;
            // 
            // checkBoxCustomer
            // 
            this.checkBoxCustomer.AutoSize = true;
            this.checkBoxCustomer.Location = new System.Drawing.Point(6, 155);
            this.checkBoxCustomer.Name = "checkBoxCustomer";
            this.checkBoxCustomer.Size = new System.Drawing.Size(70, 17);
            this.checkBoxCustomer.TabIndex = 16;
            this.checkBoxCustomer.Text = "Customer";
            this.checkBoxCustomer.UseVisualStyleBackColor = true;
            this.checkBoxCustomer.CheckedChanged += new System.EventHandler(this.checkBoxCustomer_CheckedChanged);
            // 
            // comboBoxCustomer
            // 
            this.comboBoxCustomer.Enabled = false;
            this.comboBoxCustomer.FormattingEnabled = true;
            this.comboBoxCustomer.Location = new System.Drawing.Point(109, 153);
            this.comboBoxCustomer.Name = "comboBoxCustomer";
            this.comboBoxCustomer.Size = new System.Drawing.Size(175, 21);
            this.comboBoxCustomer.TabIndex = 15;
            // 
            // checkBoxOffice
            // 
            this.checkBoxOffice.AutoSize = true;
            this.checkBoxOffice.Location = new System.Drawing.Point(6, 128);
            this.checkBoxOffice.Name = "checkBoxOffice";
            this.checkBoxOffice.Size = new System.Drawing.Size(54, 17);
            this.checkBoxOffice.TabIndex = 14;
            this.checkBoxOffice.Text = "Office";
            this.checkBoxOffice.UseVisualStyleBackColor = true;
            this.checkBoxOffice.CheckedChanged += new System.EventHandler(this.checkBoxOffice_CheckedChanged);
            // 
            // comboBoxOffice
            // 
            this.comboBoxOffice.Enabled = false;
            this.comboBoxOffice.FormattingEnabled = true;
            this.comboBoxOffice.Location = new System.Drawing.Point(109, 126);
            this.comboBoxOffice.Name = "comboBoxOffice";
            this.comboBoxOffice.Size = new System.Drawing.Size(175, 21);
            this.comboBoxOffice.TabIndex = 13;
            // 
            // checkBoxLaboratory
            // 
            this.checkBoxLaboratory.AutoSize = true;
            this.checkBoxLaboratory.Location = new System.Drawing.Point(6, 101);
            this.checkBoxLaboratory.Name = "checkBoxLaboratory";
            this.checkBoxLaboratory.Size = new System.Drawing.Size(76, 17);
            this.checkBoxLaboratory.TabIndex = 12;
            this.checkBoxLaboratory.Text = "Laboratory";
            this.checkBoxLaboratory.UseVisualStyleBackColor = true;
            this.checkBoxLaboratory.CheckedChanged += new System.EventHandler(this.checkBoxLaboratory_CheckedChanged);
            // 
            // comboBoxLaboratory
            // 
            this.comboBoxLaboratory.Enabled = false;
            this.comboBoxLaboratory.FormattingEnabled = true;
            this.comboBoxLaboratory.Location = new System.Drawing.Point(109, 99);
            this.comboBoxLaboratory.Name = "comboBoxLaboratory";
            this.comboBoxLaboratory.Size = new System.Drawing.Size(175, 21);
            this.comboBoxLaboratory.TabIndex = 11;
            // 
            // checkBoxDepartment
            // 
            this.checkBoxDepartment.AutoSize = true;
            this.checkBoxDepartment.Location = new System.Drawing.Point(6, 74);
            this.checkBoxDepartment.Name = "checkBoxDepartment";
            this.checkBoxDepartment.Size = new System.Drawing.Size(81, 17);
            this.checkBoxDepartment.TabIndex = 10;
            this.checkBoxDepartment.Text = "Department";
            this.checkBoxDepartment.UseVisualStyleBackColor = true;
            this.checkBoxDepartment.CheckedChanged += new System.EventHandler(this.checkBoxDepartment_CheckedChanged);
            // 
            // comboBoxDepartment
            // 
            this.comboBoxDepartment.Enabled = false;
            this.comboBoxDepartment.FormattingEnabled = true;
            this.comboBoxDepartment.Location = new System.Drawing.Point(109, 72);
            this.comboBoxDepartment.Name = "comboBoxDepartment";
            this.comboBoxDepartment.Size = new System.Drawing.Size(175, 21);
            this.comboBoxDepartment.TabIndex = 9;
            // 
            // checkBoxFaculty
            // 
            this.checkBoxFaculty.AutoSize = true;
            this.checkBoxFaculty.Location = new System.Drawing.Point(6, 46);
            this.checkBoxFaculty.Name = "checkBoxFaculty";
            this.checkBoxFaculty.Size = new System.Drawing.Size(60, 17);
            this.checkBoxFaculty.TabIndex = 8;
            this.checkBoxFaculty.Text = "Faculty";
            this.checkBoxFaculty.UseVisualStyleBackColor = true;
            this.checkBoxFaculty.CheckedChanged += new System.EventHandler(this.checkBoxFaculty_CheckedChanged);
            // 
            // comboBoxFaculty
            // 
            this.comboBoxFaculty.Enabled = false;
            this.comboBoxFaculty.FormattingEnabled = true;
            this.comboBoxFaculty.Location = new System.Drawing.Point(109, 44);
            this.comboBoxFaculty.Name = "comboBoxFaculty";
            this.comboBoxFaculty.Size = new System.Drawing.Size(175, 21);
            this.comboBoxFaculty.TabIndex = 7;
            // 
            // checkBoxAuthor
            // 
            this.checkBoxAuthor.AutoSize = true;
            this.checkBoxAuthor.Location = new System.Drawing.Point(6, 19);
            this.checkBoxAuthor.Name = "checkBoxAuthor";
            this.checkBoxAuthor.Size = new System.Drawing.Size(57, 17);
            this.checkBoxAuthor.TabIndex = 6;
            this.checkBoxAuthor.Text = "Author";
            this.checkBoxAuthor.UseVisualStyleBackColor = true;
            this.checkBoxAuthor.CheckedChanged += new System.EventHandler(this.checkBoxAuthor_CheckedChanged);
            // 
            // buttonApply
            // 
            this.buttonApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonApply.ForeColor = System.Drawing.Color.Maroon;
            this.buttonApply.Location = new System.Drawing.Point(205, 334);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(95, 36);
            this.buttonApply.TabIndex = 3;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // boxMenu
            // 
            this.boxMenu.Controls.Add(this.buttonApply);
            this.boxMenu.Controls.Add(this.boxMethod);
            this.boxMenu.Controls.Add(this.boxFilters);
            this.boxMenu.Location = new System.Drawing.Point(12, 27);
            this.boxMenu.Name = "boxMenu";
            this.boxMenu.Size = new System.Drawing.Size(306, 376);
            this.boxMenu.TabIndex = 6;
            this.boxMenu.TabStop = false;
            this.boxMenu.Text = "Menu";
            // 
            // richTextBoxDisplay
            // 
            this.richTextBoxDisplay.Location = new System.Drawing.Point(340, 27);
            this.richTextBoxDisplay.Name = "richTextBoxDisplay";
            this.richTextBoxDisplay.ReadOnly = true;
            this.richTextBoxDisplay.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBoxDisplay.Size = new System.Drawing.Size(448, 376);
            this.richTextBoxDisplay.TabIndex = 7;
            this.richTextBoxDisplay.TabStop = false;
            this.richTextBoxDisplay.Text = "";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(800, 24);
            this.menuStrip.TabIndex = 8;
            this.menuStrip.Text = "menuStrip1";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toHTMLToolStripMenuItem,
            this.openHTMLToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // toHTMLToolStripMenuItem
            // 
            this.toHTMLToolStripMenuItem.Name = "toHTMLToolStripMenuItem";
            this.toHTMLToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.toHTMLToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.toHTMLToolStripMenuItem.Text = "To HTML";
            this.toHTMLToolStripMenuItem.Click += new System.EventHandler(this.toHTMLToolStripMenuItem_Click);
            // 
            // openHTMLToolStripMenuItem
            // 
            this.openHTMLToolStripMenuItem.Name = "openHTMLToolStripMenuItem";
            this.openHTMLToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.openHTMLToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.openHTMLToolStripMenuItem.Text = "Open HTML";
            this.openHTMLToolStripMenuItem.Click += new System.EventHandler(this.openHTMLToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // XMLtoScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 415);
            this.Controls.Add(this.richTextBoxDisplay);
            this.Controls.Add(this.boxMenu);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "XMLtoScreen";
            this.Text = "XMLtoScreen";
            this.boxMethod.ResumeLayout(false);
            this.boxMethod.PerformLayout();
            this.boxFilters.ResumeLayout(false);
            this.boxFilters.PerformLayout();
            this.boxMenu.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonSAX;
        private System.Windows.Forms.RadioButton radioButtonDOM;
        private System.Windows.Forms.RadioButton radioButtonLINQ;
        private System.Windows.Forms.GroupBox boxMethod;
        private System.Windows.Forms.ComboBox comboBoxAuthor;
        private System.Windows.Forms.GroupBox boxFilters;
        private System.Windows.Forms.CheckBox checkBoxCustomer;
        private System.Windows.Forms.ComboBox comboBoxCustomer;
        private System.Windows.Forms.CheckBox checkBoxOffice;
        private System.Windows.Forms.ComboBox comboBoxOffice;
        private System.Windows.Forms.CheckBox checkBoxLaboratory;
        private System.Windows.Forms.ComboBox comboBoxLaboratory;
        private System.Windows.Forms.CheckBox checkBoxDepartment;
        private System.Windows.Forms.ComboBox comboBoxDepartment;
        private System.Windows.Forms.CheckBox checkBoxFaculty;
        private System.Windows.Forms.ComboBox comboBoxFaculty;
        private System.Windows.Forms.CheckBox checkBoxAuthor;
        private System.Windows.Forms.CheckBox checkBoxArea;
        private System.Windows.Forms.ComboBox comboBoxArea;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.GroupBox boxMenu;
        private System.Windows.Forms.RichTextBox richTextBoxDisplay;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toHTMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openHTMLToolStripMenuItem;
    }
}

