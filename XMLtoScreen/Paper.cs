﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace XMLtoScreen
{
    class NotEnoughDataException: Exception
    {
        public NotEnoughDataException()
        {
        }

        public NotEnoughDataException(string message): base(message)
        {
        }
        
        public NotEnoughDataException(string message, Exception innerException): base(message, innerException)
        {
        }
    }
    public class Paper
    {
        public string id = null;
        public string author = null;
        public string name = null;
        public string faculty = null;
        public string department = null;
        public string laboratory = null;
        public string office = null;
        public string officeStart = null;
        public string officeEnd = null;
        public string customer = null;
        public string subordination = null;
        public string area = null;

        public Paper() { }
        public Paper(List<string> data)
        {
            try
            {
                // If participates in comprassion will be marked with *
                id = data[0];
                author = data[1];       // * 1
                name = data[2];
                faculty = data[3];      // * 3
                department = data[4];   // * 4
                laboratory = data[5];   // * 5
                office = data[6];       // * 6
                officeStart = data[7]; 
                officeEnd = data[8];
                customer = data[9];     // * 9
                subordination = data[10]; 
                area = data[11];        // * 11
            }
            catch (IndexOutOfRangeException)
            {
                throw new NotEnoughDataException();
            }
        }

        public bool Compare(Paper p)
        {
            return (
                (id == p.id) &&
                (author == p.author) &&
                (name == p.name) &&
                (faculty == p.faculty) &&
                (department == p.department) &&
                (laboratory == p.laboratory) &&
                (office == p.office) &&
                (officeStart == p.officeStart) &&
                (officeEnd == p.officeEnd) &&
                (customer == p.customer) &&
                (subordination == p.subordination) &&
                (area == p.area)
            );
        }

        public static Dictionary<string, HashSet<string>> GetUniqueAttributes(List<Paper> listOfPapers)
        {
            Dictionary<string, HashSet<string>> result = new Dictionary<string, HashSet<string>>
            {
                { "author"      , new HashSet<string>() },
                { "faculty"     , new HashSet<string>() },
                { "department"  , new HashSet<string>() },
                { "laboratory"  , new HashSet<string>() },
                { "office"      , new HashSet<string>() },
                { "customer"    , new HashSet<string>() },
                { "area"        , new HashSet<string>() }
            };
            foreach(Paper p in listOfPapers)
            {
                result["author"]    .Add(p.author);
                result["faculty"]   .Add(p.faculty);
                result["department"].Add(p.department);
                result["laboratory"].Add(p.laboratory);
                result["office"]    .Add(p.office);
                result["customer"]  .Add(p.customer);
                result["area"]      .Add(p.area);
            }
            return result;
        }
    }
}
