﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/">
        <html>
        <body>
            <h2>Research Papers</h2>
            <table border="1">
              <tr bgcolor="cyan">
                <th>ID</th>
                <th>Author</th>
                <th>name</th>
                <th>faculty</th>
                <th>department</th>
                <th>laboratory</th>
                <th>office</th>
                <th>officeStart</th>
                <th>officeEnd</th>
                <th>customer</th>
                <th>subordiantion</th>
                <th>area</th>
              </tr>
              <xsl:for-each select="ResearchPapersDB/paper">
                <tr>
                  <td><xsl:value-of select="@id"/></td>
                  <td><xsl:value-of select="@author"/></td>
                  <td><xsl:value-of select="@name"/></td>
                  <td><xsl:value-of select="@faculty"/></td>
                  <td><xsl:value-of select="@department"/></td>
                  <td><xsl:value-of select="@laboratory"/></td>
                  <td><xsl:value-of select="@office"/></td>
                  <td><xsl:value-of select="@officeStart"/></td>
                  <td><xsl:value-of select="@officeEnd"/></td>
                  <td><xsl:value-of select="@customer"/></td>
                  <td><xsl:value-of select="@subordination"/></td>
                  <td><xsl:value-of select="@area"/></td>
                </tr>
              </xsl:for-each>
            </table>
        </body>
        </html>
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
