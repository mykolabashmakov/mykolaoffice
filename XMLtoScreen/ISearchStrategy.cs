﻿using System.Collections.Generic;

namespace XMLtoScreen
{
    public interface ISearchStrategy
    {
        List<Paper> Search(Paper filter, string path = null);

    }
}
