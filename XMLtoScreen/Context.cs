﻿using System.Collections.Generic;
using System.IO;

namespace XMLtoScreen
{
    public class Context
    {
        private ISearchStrategy _strategy;

        public Context() 
        { }

        public Context(ISearchStrategy strategy)
        {
            this._strategy = strategy;
        }

        public void SetStrategy(ISearchStrategy strategy)
        {
            _strategy = strategy;
        }

        public List<Paper> Search(Paper filter, string path = null)
        {
            if (path is null)
            {
                path = Constants.FullFilePath;
            }
            return _strategy.Search(filter, path);
        }
    }
}
