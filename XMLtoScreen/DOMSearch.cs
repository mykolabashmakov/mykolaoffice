﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace XMLtoScreen
{
    class DOMSearch : ISearchStrategy
    {
        private XmlDocument xmlDoc = new XmlDocument();
        private List<Paper> result = null;
        public List<Paper> Search(Paper filter, string path = null)
        {
            xmlDoc.Load(path);
            List<List<Paper>> table = new List<List<Paper>>();
            if (
                filter.id == null &&
                filter.author == null &&
                filter.name == null &&
                filter.faculty == null &&
                filter.department == null &&
                filter.laboratory == null &&
                filter.office == null &&
                filter.officeStart == null &&
                filter.officeEnd == null &&
                filter.customer == null &&
                filter.subordination == null &&
                filter.area == null
                )
            {
                return ErrorCatch(xmlDoc);
            }

            if (filter.author != null) table.Add(SearchByAttribute("paper", "author", filter.author, xmlDoc));
            if (filter.faculty != null) table.Add(SearchByAttribute("paper", "faculty", filter.faculty, xmlDoc));
            if (filter.department != null) table.Add(SearchByAttribute("paper", "department", filter.department, xmlDoc));
            if (filter.laboratory != null) table.Add(SearchByAttribute("paper", "laboratory", filter.laboratory, xmlDoc));
            if (filter.office != null) table.Add(SearchByAttribute("paper", "office", filter.office, xmlDoc));
            if (filter.customer != null) table.Add(SearchByAttribute("paper", "customer", filter.customer, xmlDoc));
            if (filter.area != null) table.Add(SearchByAttribute("paper", "area", filter.area, xmlDoc));

            return Cross(table, filter);
        }

        private List<Paper> ErrorCatch(XmlDocument xmlDoc)
        {
            result = new List<Paper>();
            XmlNodeList nodes = xmlDoc.SelectNodes("//paper");
            foreach(XmlNode node in nodes)
            {
                result.Add(NodeToPaper(node));
            }
            return result;
        }

        private List<Paper> Cross(List<List<Paper>> table, Paper filter)
        {
            result = new List<Paper>();
            List<Paper> clear = CheckNodes(table, filter);
            foreach(Paper p in clear)
            {
                bool isIn = false;
                foreach(Paper toCheck in result)
                {
                    if (toCheck.Compare(p))
                    {
                        isIn = true;
                    }
                }
                if (!isIn)
                {
                    result.Add(p);
                }
            }
            return result;
        }

        private List<Paper> CheckNodes(List<List<Paper>> table, Paper filter)
        {
            List<Paper> res = new List<Paper>();
            foreach(List<Paper> list in table)
            {
                foreach(Paper p in list)
                {
                    if (
                        (filter.author     == null || filter.author      == p.author) &&
                        (filter.faculty    == null || filter.faculty     == p.faculty) &&
                        (filter.department == null || filter.department  == p.department) &&
                        (filter.laboratory == null || filter.laboratory  == p.laboratory) &&
                        (filter.office     == null || filter.office      == p.office) &&
                        (filter.customer   == null || filter.customer    == p.customer) &&
                        (filter.area       == null || filter.area        == p.area)
                        )
                    {
                        res.Add(p);
                    }
                }
            }
            return res;
        }

        private List<Paper> SearchByAttribute(string nodeName, string attributeName, string value, XmlDocument xdoc)
        {
            List<Paper> found = new List<Paper>();
            if (value != null)
            {
                XmlNodeList nodes = xdoc.SelectNodes("//" + nodeName + "[@" + attributeName + "=\"" + value + "\"]");
                foreach(XmlNode node in nodes)
                {
                    found.Add(NodeToPaper(node));
                }
            }
            return found;
        }

        private Paper NodeToPaper(XmlNode node)
        {
            List<string> pData = new List<string>(12);
            pData.Add(node.Attributes.GetNamedItem("id").Value);
            pData.Add(node.Attributes.GetNamedItem("author").Value);
            pData.Add(node.Attributes.GetNamedItem("name").Value);
            pData.Add(node.Attributes.GetNamedItem("faculty").Value);
            pData.Add(node.Attributes.GetNamedItem("department").Value);
            pData.Add(node.Attributes.GetNamedItem("laboratory").Value);
            pData.Add(node.Attributes.GetNamedItem("office").Value);
            pData.Add(node.Attributes.GetNamedItem("officeStart").Value);
            pData.Add(node.Attributes.GetNamedItem("officeEnd").Value);
            pData.Add(node.Attributes.GetNamedItem("customer").Value);
            pData.Add(node.Attributes.GetNamedItem("subordination").Value);
            pData.Add(node.Attributes.GetNamedItem("area").Value);
            return new Paper(pData);
        }
    }
}
