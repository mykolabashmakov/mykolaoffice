﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace XMLtoScreen
{
    class SAXSearch : ISearchStrategy
    {
        private List<Paper> lastResult = new List<Paper>();
        public List<Paper> Search(Paper filter, string path = null)
        {
            XmlReader reader = XmlReader.Create(path);
            List<Paper> result = new List<Paper>();
            Paper found = null;

            while (reader.Read())
            {
                switch(reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (reader.Name == "paper")
                        {
                            found = new Paper();
                            while(reader.MoveToNextAttribute())
                            {
                                if (reader.Name == "id")
                                {
                                    found.id = reader.Value;
                                }
                                else if (reader.Name == "author")
                                {
                                    found.author = reader.Value;
                                }
                                else if (reader.Name == "name")
                                {
                                    found.name = reader.Value;
                                }
                                else if (reader.Name == "faculty")
                                {
                                    found.faculty = reader.Value;
                                }
                                else if (reader.Name == "department")
                                {
                                    found.department = reader.Value;
                                }
                                else if (reader.Name == "laboratory")
                                {
                                    found.laboratory = reader.Value;
                                }
                                else if (reader.Name == "office")
                                {
                                    found.office = reader.Value;
                                }
                                else if (reader.Name == "officeStart")
                                {
                                    found.officeStart = reader.Value;
                                }
                                else if (reader.Name == "officeEnd")
                                {
                                    found.officeEnd = reader.Value;
                                }
                                else if (reader.Name == "customer")
                                {
                                    found.customer = reader.Value;
                                }
                                else if (reader.Name == "subordination")
                                {
                                    found.subordination = reader.Value;
                                }
                                else if (reader.Name == "area")
                                {
                                    found.area = reader.Value;
                                }
                            }
                            result.Add(found);

                        }
                        break;
                }
            }

            lastResult = Filter(result, filter);
            return lastResult;
        }

        private List<Paper> Filter(List<Paper> result, Paper filter)
        {
            List<Paper> newResult = new List<Paper>();
            if (result != null)
            {
                foreach(Paper p in result)
                {
                    if (
                        (filter.author == null || filter.author == p.author) &&
                        (filter.faculty == null || filter.faculty == p.faculty) &&
                        (filter.department == null || filter.department == p.department) &&
                        (filter.laboratory == null || filter.laboratory == p.laboratory) &&
                        (filter.office == null || filter.office == p.office) &&
                        (filter.customer == null || filter.customer == p.customer) &&
                        (filter.area == null || filter.area == p.area)
                        )
                    {
                        newResult.Add(p);
                    }
                }
            }
            return newResult;
        }
    }
}
