﻿using System;
using System.Windows.Forms;

namespace MykolaSheet
{
    public partial class ErrorPopup : Form
    {
        public ErrorPopup(string message)
        {
            InitializeComponent();
            errorLabel.Text = message;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
