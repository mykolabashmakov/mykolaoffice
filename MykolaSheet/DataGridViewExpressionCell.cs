﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MykolaSheet
{
    using CellIndex = Tuple<string, int>;

    class DataGridViewExpressionCell : DataGridViewTextBoxCell
    {
        public string Expression { get; set; } = "";
        public HashSet<CellIndex> dependsOn = new HashSet<CellIndex>();
        public HashSet<CellIndex> dependents = new HashSet<CellIndex>();

        // TODO create Parser Locally, using DGV object
        public void eval(ref DataGridView dgv)
        {
            Parser p = new Parser(ref dgv);
            if (Expression == "")
            {
                Value = 0;
                dependsOn.Clear();
                return; // Забув ретурн XD
            }
            var cellIndex = new CellIndex(OwningColumn.Name, RowIndex);
            double value = p.Evaluate(Expression, cellIndex);
            dependsOn = p.DependsOn;
            foreach (CellIndex ci in p.DependsOn)
            {
                ((DataGridViewExpressionCell)dgv[ci.Item1, ci.Item2]).dependents.Add(cellIndex);
            }

            Value = value;
        }

        public bool IsDependsOn(ref DataGridView dgv, CellIndex cellIndex)
        {
            HashSet<CellIndex> visited = new HashSet<CellIndex>();
            Queue<CellIndex> queue = new Queue<CellIndex>();
            var index = new CellIndex(OwningColumn.Name, RowIndex);
            //visited.Add(index);
            queue.Enqueue(index);

            while (queue.Count != 0)
            {
                CellIndex vIndex = queue.Dequeue();

                var v = (DataGridViewExpressionCell)dgv[vIndex.Item1, vIndex.Item2];
                visited.Add(vIndex);
                foreach (CellIndex idx in v.dependsOn)
                {
                    if (idx.Item1 == cellIndex.Item1 && idx.Item2 == cellIndex.Item2)
                    {
                        return true;
                    }
                    else if (!visited.Contains(idx))
                    {
                        queue.Enqueue(idx);
                    }
                }
            }
            return false;
        }

        public void updateDependents(ref DataGridView dgv)
        {
            foreach (CellIndex ci in dependents)
            {
                ((DataGridViewExpressionCell)dgv[ci.Item1, ci.Item2]).eval(ref dgv);
            }
        }
    }
}
