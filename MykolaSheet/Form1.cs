﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MykolaSheet
{
    public partial class Form1 : Form
    {
        private const string WindowName = "MykolaSheet";
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string expression = expTextBox.Text.ToUpper();
            Parser p = new Parser(ref Table);

            try
            {
                ((DataGridViewExpressionCell)Table.SelectedCells[0]).Expression = expression;
                ((DataGridViewExpressionCell)Table.SelectedCells[0]).eval(ref Table);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("Firsly, Select cells");
            }
            catch (StackOverflowException ex)
            {
                MessageBox.Show("Cell depends on itself!");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Table_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            string cellValueExp = ((DataGridViewExpressionCell)Table.Rows[e.RowIndex].Cells[e.ColumnIndex]).Expression;
            expTextBox.Text = cellValueExp;
            Eval.Enabled = true;
        }

        private void result_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            addRowToTable();

        }

        private void addRowToTable()
        {
            if (Table.Columns.Count < 1)
            {
                MessageBox.Show("Firstly, add some columns!");
                return;
            }
            int rowIdx = Table.Rows.Count;

            Table.Rows.Add(1);
            Table.Rows[rowIdx - 1].HeaderCell.Value = (rowIdx - 1).ToString();
        }

        private string getNextColumnName(string name)
        {
            StringBuilder newName = new StringBuilder(name);
            // If string is empty. 
            if (name == "")
                return "A";
            // Find first character from right  
            // which is not z. 

            int i = name.Length - 1;
            while (i >= 0 && name[i] == 'Z')
            {
                --i;
            }
            // If all characters are 'z', replace them with 'a' 
            // and add an 'a' at the end. 
            if (i == -1)
            {
                newName.Replace('Z', 'A').Append('A');
            }
            // If there are some non-z characters  
            else
            {
                newName[i] = (char)(name[i] + 1);
                ++i;
                while (i <= name.Length - 1)
                {
                    newName[i] = 'A';
                    ++i;
                }
            }

            return newName.ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var col = new DataGridViewExpressionCellColumn();
            string prevName;
            if (Table.Columns.Count == 0)
            {
                prevName = "";
            }
            else
            {
                prevName = Table.Columns.GetLastColumn(DataGridViewElementStates.Visible, DataGridViewElementStates.None).Name;
            }
            col.Name = getNextColumnName(prevName);
            Table.Columns.Add(col);
        }

        private void Table_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            ((DataGridViewExpressionCell)Table[e.ColumnIndex, e.RowIndex]).updateDependents(ref Table);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "CSV (*.csv) | *.csv";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.Text = WindowName + " - " + ofd.FileName;

                this.Table.Rows.Clear();
                this.Table.Columns.Clear();
                string[] lines = System.IO.File.ReadAllLines(ofd.FileName);
                string[] headerRow = lines[0].Split(',');

                foreach (string header in headerRow)
                {
                    Table.Columns.Add(new DataGridViewExpressionCellColumn());
                    Table.Columns[Table.Columns.Count - 1].Name = header;
                }

                lines = lines.Skip(1).ToArray();
                for (var i = 0; i < lines.Length; ++i)
                {
                    addRowToTable();
                    string line = lines[i];
                    string[] cells = line.Split(',');
                    var headersAndCells = headerRow.Zip(cells, (h, c) => new { header = h, cellExp = c });
                    foreach (var hc in headersAndCells)
                    {
                        ((DataGridViewExpressionCell)Table[hc.header, i]).Expression = hc.cellExp;
                    }
                    //Console.WriteLine();
                }
                foreach (DataGridViewRow row in Table.Rows)
                {
                    foreach (DataGridViewExpressionCell cell in row.Cells)
                    {
                        if (cell.Expression != "")
                        {
                            cell.eval(ref Table);
                        }
                    }
                }
                //Console.WriteLine(lines);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "CSV (*.csv) | *.csv";
            bool fileError = false;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                this.Text = WindowName + " - " + sfd.FileName;
                if (File.Exists(sfd.FileName))
                {
                    try
                    {
                        File.Delete(sfd.FileName);
                    }
                    catch (IOException ex)
                    {
                        fileError = true;
                        MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                    }
                }
                if (!fileError)
                {
                    try
                    {
                        int columnCount = Table.Columns.Count;
                        string columnNames = "";
                        string[] outputCsv = new string[Table.Rows.Count + 1];
                        for (int i = 0; i < columnCount; i++)
                        {
                            columnNames += Table.Columns[i].HeaderText.ToString();
                            if (i < columnCount - 1)
                            {
                                columnNames += ",";
                            }
                        }
                        outputCsv[0] += columnNames;

                        for (int i = 1; (i - 1) < Table.Rows.Count; i++)
                        {
                            for (int j = 0; j < columnCount; j++)
                            {
                                outputCsv[i] += ((DataGridViewExpressionCell)Table.Rows[i - 1].Cells[j]).Expression;
                                if (j < columnCount - 1)
                                {
                                    outputCsv[i] += ",";
                                }
                            }
                        }

                        File.WriteAllLines(sfd.FileName, outputCsv, Encoding.UTF8);
                        MessageBox.Show("Data Exported Successfully !!!", "Info");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error :" + ex.Message);
                    }
                }
            }
        }
    }
}
