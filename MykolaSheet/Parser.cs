﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MykolaSheet
{
    class ParserException : Exception
    {
        public ParserException(string msg) : base(msg) { }
        public override string ToString()
        {
            return Message;
        }
    }
    public class Parser
    {
        enum Token
        {
            NONE,
            NUM,
            ID,
            OPERATOR,
            DELIMETER
        }
        enum Errors
        {
            SYNTAX,
            UNBALPARENS,
            NOEXP,
            DIVBYZERO,
            UNEXPECTED
        }
        string[] errorMsgs =
        {
            "Syntax error",
            "Unbalaced parentheses",
            "No expression",
            "Division by zero",
            "Unexpected"
        };

        private string exp;
        private int pos;
        private string token;
        private Token tokenType;
        private DataGridView dgv;
        private Tuple<string, int> cellIndex;

        public HashSet<Tuple<string, int>> DependsOn { get; } = new HashSet<Tuple<string, int>>();

        public Parser(ref DataGridView dgv)
        {
            this.dgv = dgv;
        }

        private void GetToken()
        {
            tokenType = Token.NONE;
            token = "";
            while (pos < exp.Length && Char.IsWhiteSpace(exp[pos]))
            {
                ++pos;
            }
            if (pos == exp.Length)
            {
                return;
            }
            if (IsDelim(exp[pos]))
            {
                tokenType = Token.DELIMETER;
                token += exp[pos];
                ++pos;
            }
            else if (IsOper(exp[pos]))
            {
                tokenType = Token.OPERATOR;
                token += exp[pos];
                ++pos;
            }
            else if (char.IsLetter(exp[pos]))
            {
                while (!IsDelim(exp[pos]) && !IsOper(exp[pos]))
                {
                    token += exp[pos];
                    ++pos;
                    if (pos >= exp.Length)
                    {
                        break;
                    }
                }
                if (IsOper(token))
                {
                    tokenType = Token.OPERATOR;
                }
                else
                {
                    tokenType = Token.ID;
                }
            }
            else if (char.IsDigit(exp[pos]))
            {
                while (!IsDelim(exp[pos]) && !IsOper(exp[pos]))
                {
                    token += exp[pos];
                    ++pos;
                    if (pos >= exp.Length)
                    {
                        break;
                    }
                }
                tokenType = Token.NUM;
            }
        }

        public double Evaluate(string expstr, Tuple<string, int> cellIndex)
        {
            double result;
            this.cellIndex = cellIndex;
            exp = expstr;
            pos = 0;
            DependsOn.Clear();
            try
            {
                GetToken();
                if (tokenType == Token.NONE)
                {
                    //SyntaxError(Errors.NOEXP);
                    return 0.0;
                }
                EvalExp1(out result);
                if (tokenType != Token.NONE)
                {
                    SyntaxError(Errors.SYNTAX);
                }
                return result;
            }
            catch (ParserException e)
            {
                var errorPopup = new ErrorPopup(e.Message);
                errorPopup.ShowDialog();
                DependsOn.Clear();
                // Console.WriteLine(e);
                return 0.0;
            }
        }

        private void EvalExp1(out double result)
        {
            string op;
            double partialResult;

            EvalExp2(out result);

            while ((op = token) == "+" || op == "-")
            {
                GetToken();
                EvalExp2(out partialResult);
                switch (op)
                {
                    case "-":
                        result -= partialResult;
                        break;
                    case "+":
                        result += partialResult;
                        break;
                }
            }
        }

        private void EvalExp2(out double result)
        {
            string op;
            double partialResult;
            EvalExp3(out result);

            while ((op = token) == "*" || op == "/" || op == "DIV" || op == "MOD")
            {
                GetToken();
                EvalExp3(out partialResult);
                switch (op)
                {
                    case "*":
                        result *= partialResult;
                        break;
                    case "/":
                        try
                        {
                            result /= partialResult;
                        }
                        catch (DivideByZeroException ex)
                        {
                            SyntaxError(Errors.DIVBYZERO);
                        }
                        break;
                    case "DIV":
                        result = (int)(result / partialResult);
                        break;
                    case "MOD":
                        result %= partialResult;
                        break;
                }
            }
        }

        private void EvalExp3(out double result)
        {
            string op;
            double partialResult;

            EvalExp4(out result);

            while ((op = token) == "^")
            {
                GetToken();
                EvalExp3(out partialResult);
                result = Math.Pow(result, partialResult);
            }
        }

        private void EvalExp4(out double result)
        {
            string op = "";
            if (tokenType == Token.OPERATOR && token == "+" || token == "-")
            {
                op = token;
                GetToken();
            }
            EvalExp5(out result);
            if (op == "-")
            {
                result = -result;
            }
        }

        private void EvalExp5(out double result)
        {
            if (token == "(")
            {
                GetToken();
                EvalExp1(out result);
                if (token != ")")
                {
                    SyntaxError(Errors.UNBALPARENS);
                }
                GetToken();
            }
            else
            {
                Atom(out result);
            }
        }

        private void Atom(out double result)
        {
            switch (tokenType)
            {
                case Token.ID:
                    result = GetVar(token);
                    GetToken();
                    return;

                case Token.NUM:
                    try
                    {
                        result = Double.Parse(token);
                    }
                    catch (FormatException)
                    {
                        result = 0.0;
                        SyntaxError(Errors.SYNTAX);
                    }
                    GetToken();
                    return;
                default:
                    result = 0.0;
                    SyntaxError(Errors.UNEXPECTED);
                    return;
            }
        }

        private double GetVar(string id)
        {
            id = id.ToUpper();
            Regex rCheckID = new Regex("^[A-Z]+[0-9]+$");
            if (!rCheckID.IsMatch(id))
            {
                throw new ParserException("Cell name '" + id + "' is invalid");
            }
            Regex rGetColumnName = new Regex("[^A-Z]");
            Regex rGetRowName = new Regex("[^0-9]");
            string columnName = rGetColumnName.Replace(id, "");
            int rowNum = int.Parse(rGetRowName.Replace(id, ""));

            try
            {
                var cell = (DataGridViewExpressionCell)dgv[columnName, rowNum];
                if (cell.IsDependsOn(ref dgv, cellIndex))
                {
                    throw new StackOverflowException("Cell '" + cellIndex.Item1 + cellIndex.Item2 + "'`s expression goes cycle");
                }
                cell.eval(ref dgv);
                var cellVal = cell.Value;
                string valString = cellVal.ToString();
                double val;
                if (valString == "")
                {
                    val = 0.0;
                }
                else
                {
                    val = double.Parse(valString);
                }
                DependsOn.Add(new Tuple<string, int>(columnName, rowNum));
                return val;
            }
            catch (ArgumentException e)
            {
                throw new ParserException("Cell '" + id + "' does not exists");
            }
            catch (NullReferenceException e)
            {
                throw new ParserException("Cell '" + id + "' is empty");
            }
            catch (FormatException e)
            {
                throw new ParserException("Cell '" + id + "' contains not double");
            }
        }

        private void SyntaxError(Errors error)
        {
            throw new ParserException(errorMsgs[(int)error]);
        }

        private bool IsDelim(char c)
        {
            return " ()".IndexOf(c) != -1;
        }

        private bool IsOper(string s)
        {
            return (new List<string> { "DIV", "MOD", "+", "-", "/", "*", "^" }).Contains(s);
        }

        private bool IsOper(char c)
        {
            return "+-^*/".IndexOf(c) != -1;
        }
    }
}
