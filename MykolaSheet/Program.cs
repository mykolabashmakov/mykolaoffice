﻿using System;
using System.Windows.Forms;

namespace MykolaSheet
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var mainWindow = new Form1();
            Application.Run(mainWindow);
        }
    }
}
