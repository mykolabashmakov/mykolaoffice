﻿using System.Windows.Forms;

namespace MykolaSheet
{
    class DataGridViewExpressionCellColumn : DataGridViewColumn
    {
        public DataGridViewExpressionCellColumn()
        {
            this.CellTemplate = new DataGridViewExpressionCell();
        }
    }
}
