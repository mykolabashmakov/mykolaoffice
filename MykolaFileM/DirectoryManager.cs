﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MykolaFileM
{
    class DirectoryManager
    {
        private string currentDirectory = GetAllDrives()[0];
        public string CurrentDirectory
        {
            get
            {
                return currentDirectory;
            }
            set
            {
                /* TODO: If(IsDirAvialable(value)){ */
                       currentDirectory = value;
                /* }
                 */
            }
        }

        public DirectoryManager(string defDir = null)
        {
            if (defDir != null)
                CurrentDirectory = defDir;
        }

        public List<string> GetAllSubDirectoryNames()
        {
            string[] dirs;
            dirs = Directory.GetDirectories(currentDirectory);
            for (int i = 0; i < dirs.Length; i++)
            {
                string dir_ = dirs[i];
                dirs[i] = new DirectoryInfo(dir_).Name;
            }
            return dirs.OfType<string>().ToList();
        }

        public List<string> GetAllFileNames()
        {
            string[] files;
            try
            {
                files = Directory.GetFiles(currentDirectory);
                for (int i = 0; i < files.Length; i++)
                {
                    string file_ = files[i];
                    files[i] = new FileInfo(file_).Name;
                }
                return files.OfType<string>().ToList();
            }
            catch (UnauthorizedAccessException)
            {
                // Need to run with admin rights
            }
            return new List<string>();
        }

        public void MoveFiles(dynamic files, string dst)
        {
            foreach(var file in files)
            {
                try
                {
                    Directory.Move(currentDirectory + "\\\\" + file, dst + "\\\\" + file);
                } catch (Exception ex)
                {

                }
            }
        }


        public static List<string> GetAllDrives()
        {
            string[] drives;
            drives = Directory.GetLogicalDrives();
            return drives.OfType<string>().ToList();
        }

        public void OpenFiles(dynamic selectedItems)
        {
            string args = "";
            foreach(var selected_item in selectedItems) {
                args += $" {currentDirectory}\\{selected_item}";
            }

            Process.Start("C:\\Users\\MrGra\\source\\repos\\MykolaOffice\\MykolaEdit\\bin\\Release\\MykolaEdit.exe", args);
        }
    }
}
