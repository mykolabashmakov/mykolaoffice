﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MykolaFileM
{
    public partial class Form1 : Form
    {
        List<DirectoryManager> dms = new List<DirectoryManager> { new DirectoryManager(), new DirectoryManager() };

        public Form1()
        {
            InitializeComponent();
            InitializeSearchGroupBox(groupBox1);
            InitializeSearchGroupBox(groupBox2);
        }

        private void InitializeTreeView(TreeView tv, string rootDir = null)
        {
            tv.Nodes.Clear();
            if (rootDir == null)
            {
                List<string> list = DirectoryManager.GetAllDrives();
                rootDir = list.First();
            }
            var node = new TreeNode { Text = rootDir };
            tv.Nodes.Add(node);
            NodeBuildTree(node);
        }

        private void InitializeDriveSelectBox(ComboBox cb)
        {
            cb.Items.Clear();
            var drives = DirectoryManager.GetAllDrives();
            foreach(var drive in drives)
            {
                cb.Items.Add(drive);
            }
        }

        private void InitializeFilterSelectBox(ComboBox cb)
        {
            cb.Items.Clear();
            cb.DisplayMember = "Text";
            cb.ValueMember = "Value";
            var filters = new Dictionary<string, string> { { "All files", "^.+$" } };
            var formats = new List<string> { "txt", "HTML", "XML"};
            foreach (var format in formats)
            {
                filters.Add($"{format} file (*.{format.ToLower()})", $"^.*.{format}$");
            }

            var anyEditableRegex = $"^.*.(?:{String.Join("|", formats)})$";
            filters.Add("Any editable", anyEditableRegex);

            foreach (var filter in filters)
            {
                cb.Items.Add(new { Text = filter.Key, Value = filter.Value});
            }

            cb.SelectedIndex = 0;
        }


        private void InitializeSearchGroupBox(GroupBox gb, string dir = null)
        {
            var comboBoxes = GetAll(gb, typeof(ComboBox)).ToList();
            
            dynamic driveSelect = comboBoxes.Where((cb) => (string)(cb.Tag) == "Drive").First();
            dynamic tv = GetAll(gb, typeof(TreeView)).First();
            dynamic filterSelect = comboBoxes.Where((cb) => (string)(cb.Tag) == "Filter").First();

            InitializeDriveSelectBox(driveSelect);
            InitializeFilterSelectBox(filterSelect);
            driveSelect.SelectedIndex = 0;
            InitializeTreeView(tv, driveSelect.SelectedItem.ToString());
        }

        private void NodeBuildTree(TreeNode node, int depth=0)
        {
            if (depth >= 2)
                return;
            node.Nodes.Clear();
            List<string> dirs;
            var dm = new DirectoryManager(node.FullPath);
            try
            {
                dirs = dm.GetAllSubDirectoryNames();
                for (int i = 0; i < dirs.Count; i++)
                {
                    TreeNode dirNode = new TreeNode(dirs[i]);
                    node.Nodes.Add(dirNode);
                    NodeBuildTree(dirNode, depth + 1);
                }
            }
            catch (Exception ex) { }
        }

        private void FillWithFilenamesFromDirectoryManager(GroupBox gb, DirectoryManager dm)
        {
            dynamic lb = GetAll(gb, typeof(ListBox)).First();
            dynamic regexComboBox = GetAll(gb, typeof(ComboBox)).ToList().Where((cb) => (string)(cb.Tag) == "Filter").First();

            string regexFilter = "";
            if (regexComboBox.SelectedItem == null)
            {
                regexFilter = $"^.*{regexComboBox.Text.ToLower()}.*$";
            } else
            {
                regexFilter = regexComboBox.SelectedItem.Value.ToLower();
            }

            lb.Items.Clear();
            var filenames = dm.GetAllFileNames();
            var regex = new Regex(regexFilter);
            foreach (var fn in filenames)
            {
                if (regex.IsMatch(fn.ToLower()))
                    lb.Items.Add(fn);
            }
        }

        private void treeView1_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            dms[0].CurrentDirectory = e.Node.FullPath;
            NodeBuildTree(e.Node);
        }

        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        private void treeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            NodeBuildTree(e.Node);
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            FillWithFilenamesFromDirectoryManager(groupBox1, dms[0]);
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            InitializeTreeView(treeView1, ((ComboBox)sender).SelectedItem.ToString());
            treeView1.Select();
        }

        private void comboBox3_TextChanged(object sender, EventArgs e)
        {
            FillWithFilenamesFromDirectoryManager(groupBox1, dms[0]);
        }

        private void moveSelectedFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoveFiles(groupBox1, dms[0], dms[1]);
        }

        private void MoveFiles(GroupBox gb1, DirectoryManager dm1, DirectoryManager dm2)
        {
            dynamic lb1 = GetAll(gb1, typeof(ListBox)).First();
            dm1.MoveFiles(lb1.SelectedItems, dm2.CurrentDirectory);
            FillWithFilenamesFromDirectoryManager(groupBox1, dms[0]);
            FillWithFilenamesFromDirectoryManager(groupBox2, dms[1]);
        }

        private void treeView2_AfterSelect(object sender, TreeViewEventArgs e)
        {
            FillWithFilenamesFromDirectoryManager(groupBox2, dms[1]);
        }

        private void treeView2_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            dms[1].CurrentDirectory = e.Node.FullPath;
            NodeBuildTree(e.Node);
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            InitializeTreeView(treeView2, ((ComboBox)sender).SelectedItem.ToString());
            treeView2.Select();
        }

        private void comboBox4_TextChanged(object sender, EventArgs e)
        {
            FillWithFilenamesFromDirectoryManager(groupBox2, dms[1]);
        }

        private void leftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dms[0].OpenFiles(listBox1.SelectedItems);
        }

        private void rightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dms[1].OpenFiles(listBox2.SelectedItems);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            MoveFiles(groupBox1, dms[0], dms[1]);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            MoveFiles(groupBox2, dms[1], dms[0]);
        }
    }
}
